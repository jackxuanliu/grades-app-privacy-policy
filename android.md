# Grades App Privacy Policy (Android)



### How/Why does the app use my email and password?

The credentials inputted into the login screen are used by the Grades app to log in to your Aeries account. The Grades app does not send your credentials anywhere except for your district's Aeries servers. For your convenience, your credentials are securely stored on your device's internal storage. Android is designed to ensure that no app or process on your device except for the Grades app is able to access contents in internal storage. The Grades app only uses stored credentials to facilitate auto logins.



### Advertising/Firebase Information

The Grades app uses Google's AdMob network to serve ads and Google's Firebase service to collect and analyze app usage data. AdMob and Firebase may collect anonymous data from your device, including your Android Advertising ID. For more information about what information Google's services collect, please read Google's policies [here](https://policies.google.com/privacy) and [here](https://policies.google.com/technologies/partner-sites).



------

\* Aeries is a trademark of Eagle Software. Grades is not affiliated with or endorsed by Eagle Software.
\*\* AdMob and Firebase are services provided by Google LLC. Android is a trademark of Google LLC. Grades is not affiliated with or endorsed by Google LLC.